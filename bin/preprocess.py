import pandas as pd
import os
import argparse
import subprocess
import unicodedata
import re
from conversion_tools import *
import math
conversion_table = {'ffn':[{'ffn'}, {'gbk'}, {'fna', 'gff'}],
'fna':[{'fna'}, {'gbk'}],
'gff':[{'gff'}, {'gbk'}],
'gbk':[{'gbk'}, {'gff', 'fna'}]}
conversion_fxn_dict = {'ffn' : {'ffn': identity,'gbk': convert_gbk_to_ffn,'fna_gff': convert_gff_fna_to_ffn},
'fna': {'fna':identity, 'gbk':convert_gbk_to_fna},
'gff': {'gff':identity, 'gbk':convert_gbk_to_gff},
'gbk': {'gbk':identity, 'fna_gff':convert_fasta_gff_to_gbk}}

def slugify(value):
    """
    Normalizes string, converts to lowercase, removes non-alpha characters,
    and converts spaces to underscore.
    Adjusted from slack overflow code I couldn't find again.
    """
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    value = re.sub('[^\w\s-]', '', value.decode("utf-8", "strict")).strip().lower()
    value = re.sub('[-\s]+', '_', value)
    return value


def parse_gdir(gdir,output_directory = 'test', filename = 'genomes'):
    gfiles = [os.path.join(gdir, f) for f in os.listdir(gdir)]
    genomes = ['.'.join(os.path.basename(gfile).split(".")[:-1]) for gfile in gfiles]
    meta = pd.DataFrame({'genome_name' : genomes, 'genome_file' : gfiles})
    meta_loc = os.path.join(output_directory,filename+'.p')
    meta.to_pickle(meta_loc)
    meta.to_csv(os.path.join(output_directory,filename+'.csv'))
    return(meta)

def parse_gtable(genomes):
    """
    Expect to take a two column table of genome_name, genome_file.
    """
    meta = pd.read_table(genomes, Names = ['genome_name', 'genome_file'] )
    #check names
    meta['genome_name_og'] = meta['genome_name']
    meta['genome_name'] = [slugify(g) for g in meta['genome_name']]
    #check files
    for r in meta['genome_file']:
        assert os.path.isfile(r), r+" does not exist"
    meta_loc = filename+'.p'
    meta.to_pickle(meta_loc)
    return(meta)
def resolve_filetype(meta, filetype, genome, genome_directory):
    compatible_files = conversion_table[filetype]
    file_dict = {k:v for k,v in zip(meta['filetype'], meta['genome_file'])}
    for c_set in compatible_files:
        if c_set.issubset(set(meta['filetype'])):
            c_string = '_'.join(sorted(list(c_set)))
            out = conversion_fxn_dict[filetype][c_string](file_dict,genome, os.path.join(genome_directory, 'genomes'), filetype)
            return(out)
    return('')

def parse_genomes(meta,genome_directory,files_needed = ['ffn','fna', 'gff']):
    meta['filetype'] = [r.split( '.')[-1] for r in meta['genome_file']]
    meta['filetype'] = ['gbk' if f  in {'gbk', 'gbff', 'gb'} else f for f in meta['filetype']]
    meta['filetype'] = ['gff' if f in {'gff', 'gff2', 'gff3', 'gtf'} else f  for f in meta['filetype']]
    meta['filetype'] = ['fna' if f in {'fna', 'fasta'} else f  for f in meta['filetype']]
    meta['filetype'] = ['ffn' if f in {'ffn'} else f for f in meta['filetype']]
    genome_dict = collections.defaultdict(list)
    for slice in meta.groupby(by = 'genome_name', axis = 0):
        for filetype in files_needed:
            new_file = resolve_filetype(slice[1], filetype,slice[0],  genome_directory)
            assert len(new_file)>0, slice[0]+" doesn't have sufficient information to generate "+filetype
            genome_dict[filetype]+=[new_file]
    return genome_dict






def parse_metadata(genomes,genome_directory = 'test', files_needed = ['ffn','fna', 'gff']):
    #test if directory or table
    if os.path.isfile(genomes):
        meta = parse_gtable(genomes)
    elif os.path.isdir(genomes):
        meta = parse_gdir(genomes, output_directory = genome_directory)
    elif os.path.exists(genomes):
        print('genome path exists but is not a directory or file')
    else:
        #?????? not a file or directory
        meta = 'failed'
        print('genome path does not exist')
        return(None)
    genome_dict = parse_genomes(meta,genome_directory, files_needed = ['ffn','fna', 'gff'])
    return(genome_dict)


def parse_map(map, filename = 'manifest', read_dir = '', paired = False, output_directory = 'test'):
    #generally read a delimited table
    assert os.path.isfile(map), 'file does not exist'
    manifest = pd.read_csv(map, sep = None,engine='python')
    assert len(manifest.columns) ==3, 'Manifest should have 3 columns'
    manifest.columns = ['sample_id','r1', 'r2']
    #check name compatibility
    manifest['sample_id_og'] = manifest['sample_id']
    manifest['sample_id'] = [slugify(s) for s in manifest['sample_id']]
    manifest['sample_uid'] = [s+'_'+str(i) for i,s in enumerate(manifest['sample_id'])]
    #add read_dir if needed
    manifest['r1'] = [os.path.join(read_dir, r) for r in manifest['r1']]
    if paired:
        manifest['r1t'] = [os.path.join(output_directory,'trim/',x)+'_val_1.fq' for x in manifest['sample_uid']]
    else:
        manifest['r1t'] = [os.path.join(output_directory,'trim/',x)+'_trimmed.fq' for x in manifest['sample_uid']]
    manifest['r1t'] = [t+'.gz' if r.find('.gz')>-1 else t for r,t in zip(manifest['r1'], manifest['r1t'])]
    if paired:
        manifest['r2'] = [os.path.join(read_dir, r) for r in manifest['r2']]
        manifest['r2t'] = [os.path.join(output_directory,'trim/',x)+'_val_2.fq' for x in manifest['sample_uid']]
        manifest['r2t'] = [t+'.gz' if r.find('.gz')>-1 else t for r,t in zip(manifest['r2'], manifest['r2t'])]
    #check that files exist
    for r in manifest['r1']:
        assert os.path.isfile(r), r+" does not exist"
    if paired:
        for r in manifest['r2']:
            assert os.path.isfile(r), r+" does not exist"
    #save dataframe or other object for use during sample processing
    manifest_loc = os.path.join(output_directory, filename+'.p')
    manifest.to_pickle(manifest_loc)
    #return the location of this processed manifest
    return(manifest)

def build_kallisto(genome_dict,genome_directory = 'test', index_name = 'index', kmer_size = 31):
    ffns = genome_dict['ffn']
    index_cmd = ['kallisto', 'index', '-i',os.path.join(genome_directory,index_name),'-k', str(kmer_size)]+ffns
    with open(os.path.join(genome_directory, 'index_build.out'), 'w') as f:
        proc = subprocess.Popen(index_cmd, stdout=f, stderr=subprocess.STDOUT)
        proc.wait()
    return(index_name)

def get_length_and_ncontigs(fna_list):
    genome_length = 0
    ncontigs = 0
    for fna in fna_list:
        for rec in SeqIO.parse(fna, 'fasta'):
            ncontigs+=1
            genome_length+=len(rec.seq)
    return((genome_length, ncontigs))
def build_star(genome_dict,genome_directory, index_name = 'index', num_threads = 1,read_length=75):
    fnas = genome_dict['fna']
    gffs = genome_dict['gff']
    genome_length, ncontigs = get_length_and_ncontigs(fnas)
    if not os.path.exists(os.path.join(args.genome_directory, 'star')):
        os.mkdir(os.path.join(args.genome_directory, 'star'))
    sa = min(14, int(math.log2(genome_length)/2-1))
    nbits = min(18, int(math.log2(max(genome_length/ncontigs,read_length))))
    index_cmd = ['STAR', '--runMode', 'genomeGenerate','--genomeSAindexNbases',str(sa),'--genomeDir', os.path.join(genome_directory,'star'),'--alignIntronMax', '1','--genomeChrBinNbits',str(nbits),'--runThreadN',str(num_threads),'--genomeFastaFiles']+fnas
    with open(os.path.join(genome_directory, 'star', 'star_log.out'), 'w') as f:
        proc = subprocess.Popen(index_cmd, stdout=f, stderr=subprocess.STDOUT)
        proc.wait()
    return(index_name)

def build_index(genome_dict, aligner, genome_directory):
    if aligner == 'kallisto':
        return(build_kallisto(genome_dict, genome_directory))
    elif aligner == 'star':
        return(build_star(genome_dict, genome_directory))
    else:
        assert False, "No valid aligner"

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-m",'--map',help="[STRING] Input mapping file",  required=True)
    parser.add_argument("-g",'--genomes',help="[STRING] Genome directory or table",  required=True)
    parser.add_argument("-o",'--genome_directory',help="[STRING] Output Genome directory",  required=False, default = './genomes')
    parser.add_argument("-s","--stranded",help="[0/1] Stranded?",  required=False, default = 0)
    parser.add_argument("-r","--read_dir",help="Read directory",  required=False, default = '')
    parser.add_argument("-a","--aligner",help="Aligner",  required=False, default = 'kallisto')
    parser.add_argument("-t","--num_threads", type=int,required=False, help="Number of threads")
    args = parser.parse_args()
    if not os.path.exists(args.genome_directory):
        os.mkdir(args.genome_directory)
    if not os.path.exists(os.path.join(args.genome_directory, 'genomes')):
        os.mkdir(os.path.join(args.genome_directory, 'genomes'))
    manifest = parse_map(args.map, read_dir = args.read_dir)
    genome_dictionary = parse_metadata(args.genomes, args.genome_directory)
    index_name = build_index(genome_dictionary, args.aligner, args.genome_directory)
