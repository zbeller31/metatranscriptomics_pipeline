import pickle
import pandas as pd
import subprocess
import argparse
import os
def trim_reads(sample_id, manifest,paired = False, trim_out = 'trim.out',fastq_dir = '',  trimmed_dir = 'trim', threads=1, output_directory = 'test'):
    reads = manifest.loc[manifest['sample_id'].eq(sample_id)]
    r1s = list(reads['r1'])
    r2s = list(reads['r2'])
    trim_cmd = ['trim_galore', '--fastqc', '--phred33','--output_dir', os.path.join(output_directory,trimmed_dir),'--basename']
    trim_cmd = [trim_cmd+[id] for id in reads['sample_uid']][0]
    if paired:
        trim_cmdp = trim_cmd+['--paired']
        trim_cmds = [trim_cmdp+[one,two] for one, two in zip(r1s, r2s)]
    else:
        trim_cmds = [trim_cmd+[r] for r in r1s]
    for cmd in trim_cmds:
        print(cmd)
    with open(os.path.join(output_directory, trimmed_dir,trim_out), 'w') as tout:
        trim_list = [subprocess.Popen(cmd, stdout=tout, stderr=subprocess.STDOUT) for cmd in trim_cmds]
    for proc in trim_list:
        proc.wait()


def kallisto_quant(sample_id,map, index,paired = False, num_threads =1,quant_out = 'quant.out',out_directory = 'test', fragment_length =200, fragment_sd = 30):
    sav_dir = os.path.join(out_directory, 'quant')
    if not os.path.exists(sav_dir):
        os.mkdir(sav_dir)
    quant_cmd = ['kallisto','quant', '-i',os.path.join(out_directory,index), '-o', os.path.join(sav_dir,sample_id), '-t', str(num_threads), '-l',str(fragment_length), '-s', str(fragment_sd)]
    reads = map.loc[map['sample_id'].eq(sample_id)]
    r1s = list(reads['r1t'])
    if paired:
        r2s = list(reads['r2t'])
        quant_cmds = [quant_cmd+[one, two] for one, two in zip(r1s, r2s)][0]
    else:
        quant_cmd+=['--single']
        quant_cmds = [quant_cmd+[one] for one in r1s][0]
    print(quant_cmds)
    with open(os.path.join(sav_dir,sample_id+'_'+quant_out), 'w') as qfile:
        quant_proc = subprocess.Popen(quant_cmds, stdout=qfile, stderr=subprocess.STDOUT)
        quant_proc.wait()

def star_map(sample_id,map, index,paired = False, num_threads=1,out_directory = 'test', quant_out = 'quant.out'):
    sav_dir = os.path.join(out_directory, 'mapped')
    if not os.path.exists(sav_dir):
        os.mkdir(sav_dir)
    reads = map.loc[map['sample_id'].eq(sample_id)]
    r1s = list(reads['r1t'])
    r2s = list(reads['r2t'])
    if r1s[0].find('.gz')>-1:
        star_cmd =['STAR', '--runThreadN', str(num_threads), '--genomeDir',os.path.join(index,'star') ,'--outFileNamePrefix',os.path.join(savdir, sample_id), '--readFilesCommand', 'zcat', '--readFilesIn']
    else:
        star_cmd =['STAR', '--runThreadN', str(num_threads), '--genomeDir',os.path.join(index,'star') ,'--outFileNamePrefix',os.path.join(savdir, sample_id) ,  '--readFilesIn']
    if paired:
        star_cmd1 = ','.join([one for one in r1s])
        star_cmd2 = ','.join([two for two in r2s])
        star_cmds = star_cmd+[star_cmd1, star_cmd2]
    else:
        star_cmds = star_cmd+[','.join([one for one in list(r1s)])]
    with open(os.path.join(out_directory,quant_out), 'w') as qfile:
        quant_proc = subprocess.Popen(star_cmds, stdout=qfile, stderr=subprocess.STDOUT)
        quant_proc.wait()
    return(os.path.join(out_directory,'sam', sample_id)+'_Aligned.out.sam')

def map_reads(sample_id, map, index,genome_file, pair = False, method = 'kallisto', threads = 1):
    if method == 'kallisto':
        kallisto_quant(sample_id,map, index,paired = pair, num_threads=threads)
    elif method == 'star':
        bam = star_map(sample_id,map, index,paired = pair, num_threads =threads)
        gdf = pd.read_pickle(genome_dict)
        for gff in gdf['gff']:
            count_file = feature_count(bam,gff, count_dir = 'counts',qual = 0, stranded = 'yes', threads=1)
    return(True)

def feature_count(bam_file, gff_file,count_dir, qual = 0, stranded = 'yes', threads=1):
    htseq_cmd =['htseq-count', '-f', 'bam', '-r', 'pos', '-s', stranded,'--minaqual', str(qual),'-t', 'CDS', '-i', 'locus_tag', '-m', 'union', '--nonunique', 'none']+bam_file+gff_file
    output_name = os.path.join(quant_dir,os.path.basename(bam_file).split('.')[:-1]+'_'+os.path.basename(gff_file).split('.')[:-1]'.counts')
    with open(output_name, 'r') as fcout:
        htseq_proc = subprocess.Popen(htseq_cmd, stdout=fcout, stderr=subprocess.STDOUT)
        htseq_proc.wait()
    return(output_name)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Process FASTQ files for one sample")
    parser.add_argument("-m","--map", type=str, required=True, help="Pickled Mapping file")
    parser.add_argument("-g","--genome_file", type=str, required=True, help="Pickled Metadata file")
    parser.add_argument("-s","--sample_id", type=str,required=True, help="Sample ID")
    parser.add_argument("-i","--index", type=str,required=True, help="Alignment index")
    parser.add_argument("-1","--read1", type=str,required=False,default=None, help="Sample ID")
    parser.add_argument("-2","--read2", type=str,required=False,default=None, help="Sample ID")
    parser.add_argument("--trim_dir", type=str,required=False,default='trim', help="Sample ID")
    parser.add_argument( "--paired", action = 'store_true',default=False, help="Paired")
    parser.add_argument("-a", "--aligner", type=str,required=False,default='kallisto', help="How many reads?")
    parser.add_argument("-t","--num_threads", type=int,required=False,default = 1, help="Number of threads")
    args = parser.parse_args()

    map = pickle.load( open( args.map, "rb" ) )
    trim_reads(args.sample_id, map, threads=args.num_threads, trimmed_dir = args.trim_dir)
    map_reads(args.sample_id, map, args.index, genome_file = args.genome_file,pair = args.paired, method = args.aligner, threads=args.num_threads)
