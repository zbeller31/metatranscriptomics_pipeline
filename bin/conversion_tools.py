import collections
from Bio import SeqIO
from Bio import Alphabet
from BCBio import GFF
import os
from shutil import copyfile



def identity(indict,  genome, genome_directory, filetype):
    src = indict[filetype]
    out = os.path.join(genome_directory, genome+filetype)
    copyfile(src, out)
    return out

def convert_gbk_to_fna(indict,  genome, genome_directory, filetype='fna'):
    """
    """
    gbk_file = indict['gbk']
    outfile = os.path.join(genome_directory, genome+ '.fna')
    with open(outfile, 'w') as ouf:
        for seq_record in SeqIO.parse(gbk_file, "genbank") :
            ouf.write(">%s %s %s\n%s\n" % (genome,
            seq_record.id,
            seq_record.description,
            seq_record.seq))
    return(outfile)

def convert_gbk_to_gff(indict,  genome, genome_directory, filetype='gff'):
    """

    """
    gbk_file = indict['gbk']
    gff_file = os.path.join(genome_directory, genome+ '.gff')
    with open(gff_file, "w") as gff_handle:
        GFF.write(SeqIO.parse(gbk_file, "genbank"), gff_handle)
    return(gff_file)
def convert_fasta_gff_to_gbk(indict,  genome, genome_directory, filetype='gbk'):
    """
    """
    fasta_file = indict['gbk']
    gff_file = indict['gff']
    gbk = os.path.join(genome_directory,genome+ '.gbk')
    seq_dict = SeqIO.to_dict(SeqIO.parse(fasta_file, "fasta", alphabet = Alphabet.DNAAlphabet()))
    rec_iter = (rec for rec in GFF.parse(gff_file, base_dict=seq_dict))
    SeqIO.write(rec_iter, gbk, "genbank")
    return(gbk)
def convert_gff_fna_to_ffn(indict,  genome, genome_directory, filetype='ffn'):
    if convert_fasta_gff_to_gbk(indict,  genome, genome_directory):
        indict['gbk'] = os.path.join(genome_directory,genome+ '.gbk')
        out = convert_gbk_to_ffn(indict,  genome, genome_directory)
    return(out)


def convert_gbk_to_ffn( indict, genome, genome_directory, filetype='ffn'):
    gbk_file = indict['gbk']
    ffname = os.path.join(genome_directory, genome+ '.ffn')
    feature_types = collections.Counter()
    with open(ffname, 'w') as output_handle:
        for seq_record in SeqIO.parse(gbk_file, "genbank") :
            for seq_feature in seq_record.features:
                feature_types[seq_feature.type]+=1
                if seq_feature.type in {'CDS', 'tRNA', 'rRNA', 'tmRNA', 'ncRNA'}:
                    output_handle.write(">{} {} {}\n{}\n".format(seq_feature.qualifiers['locus_tag'][0], seq_record.name,seq_feature.type, seq_feature.extract(seq_record.seq)))
    return(ffname)
