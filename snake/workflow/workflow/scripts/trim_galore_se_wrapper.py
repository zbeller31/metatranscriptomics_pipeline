"""Snakemake wrapper for trimming unpaired reads using trim_galore."""

__author__ = "Kerrin Mendler"
__copyright__ = "Copyright 2018, Kerrin Mendler"
__email__ = "mendlerke@gmail.com"
__license__ = "MIT"


from snakemake.shell import shell
import os.path
def basename_without_ext(file_path):
    """Returns basename of file path, without the file extension."""

    base = os.path.basename(file_path)

    split_ind = 2 if base.endswith(".fastq.gz") else 1
    base = ".".join(base.split(".")[:-split_ind])

    return base

log = snakemake.log_fmt_shell()

n = len(snakemake.input)
assert n == 1, "Input must contain 1 (single-end) element."

# Don't run with `--fastqc` flag
if "--fastqc" in snakemake.params.get("extra", ""):
    raise ValueError(
        "The trim_galore Snakemake wrapper cannot "
        "be run with the `--fastqc` flag. Please "
        "remove the flag from extra params. "
        "You can use the fastqc Snakemake wrapper on "
        "the input and output files instead."
    )

# Check that two output files were supplied
m = len(snakemake.output)
assert m == 2, "Output must contain 2 files. Given: %r." % m

# Check that all output files are in the same directory
out_dir = os.path.dirname(snakemake.output[0])
pre_out_dir = out_dir+"/"+snakemake.params.unit+"/"
for file_path in snakemake.output[1:]:
    assert out_dir == os.path.dirname(file_path), (
        "trim_galore can only output files to a single directory."
        " Please indicate only one directory for the output files."
    )
output_base = basename_without_ext(snakemake.input[0])
shell(
    "ml trim_galore;"
    "(trim_galore"
    " {snakemake.params.extra}"
    " -o {pre_out_dir}"
    " {snakemake.input}) > {snakemake.log};"
"mv {pre_out_dir}{output_base}_trimmed.fq.gz {snakemake.output.fq};"
"mv {pre_out_dir}{output_base}.fastq.gz_trimming_report.txt {snakemake.output.report};"
)
