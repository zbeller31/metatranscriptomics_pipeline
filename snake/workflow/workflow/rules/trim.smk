if config['paired']:
    ruleorder: fastp_pe > fastp_se
else:
    ruleorder: fastp_se > fastp_pe

# ruleorder: fastqc_pre_pe > fastqc_pre_se

RESULTDIR=config['resultdir']
# rule fastqc_pre_se:
#     input:
#         get_fastqs
#     output:
#         html=RESULTDIR+"/qc/fastqc_pre/{sample}-{unit}.html",
#         zip=RESULTDIR+"/qc/fastqc_pre/{sample}-{unit}_fastqc.zip" 
#         # the suffix _fastqc.zip is necessary for multiqc to find the file.
#     params: ""
#     log:
#         RESULTDIR+"/logs/fastqc_pre/{sample}-{unit}.log"
#     threads: config["params"]["fastqc"]["threads"]
#     resources:
#         mem_mb = config['params']['fastqc']['mem_mb']
#     script:
#         "../scripts/fastqc_wrapper_se.py"
        
        
# rule trim_galore_se:
#     input:
#         get_fastqs
#     output:
#         fq=RESULTDIR+"/trimmed/{sample}-{unit}.fastq.gz",
#         report=RESULTDIR+"/trimmed/{sample}-{unit}.fastq.gz_trimming_report.txt"
#     params:
#         extra=config["params"]["trim_galore"]["extra"], unit = "{unit}"

#     threads: config["params"]["trim_galore"]["threads"]
#     resources:
#         mem_mb = config['params']['trim_galore']['mem_mb']
#     log:
#         RESULTDIR+"/logs/trim_galore/{sample}-{unit}.log"
#     script:
#         "../scripts/trim_galore_se_wrapper.py"
        
# rule trim_galore_pe:
#     input:
#         get_fastqs
#     output:
#         fq1 = RESULTDIR+"/trimmed/{sample}-{unit}-1.fastq.gz",
#         rep1 = RESULTDIR+"/trimmed/{sample}-{unit}.1.fastq.gz_trimming_report.txt",
#         rep2 = RESULTDIR+"/trimmed/{sample}-{unit}.2.fastq.gz_trimming_report.txt",
#         fq2 = RESULTDIR+"/trimmed/{sample}-{unit}-2.fastq.gz"
#     params:
#         extra = config["params"]["trim_galore"]["extra"],unit = "{unit}"
#     threads: config["params"]["trim_galore"]["threads"]
#     resources:
#         mem_mb = config['params']['trim_galore']['mem_mb']
#     log:
#         RESULTDIR+"/logs/trim_galore/{sample}-{unit}.log"
#     script:
#         "../scripts/trim_galore_pe_wrapper.py"
        
rule fastp_se:
    input:
        fq = get_fastqs
    output:
        fq=RESULTDIR+"/trimmed/{sample}-{unit}.fastq.gz",
        json=RESULTDIR+"/qc/{sample}-{unit}.fastp.json",
        html=RESULTDIR+"/qc/{sample}-{unit}.html"
    params:
        extra =config["params"]["fastp"]["extra"] 
    resources:
        mem_mb = config["params"]["fastp"]["mem_mb"]
    threads:
        config["params"]["fastp"]["threads"]
    log:
        RESULTDIR+"/logs/fastp/{sample}-{unit}.log"
    script:
        "../scripts/fastp_se.py"
        
rule fastp_pe:
    input:
        fq = get_fastqs
    output:
        fq1=RESULTDIR+"/trimmed/{sample}-{unit}-1.fastq.gz",
        fq2=RESULTDIR+"/trimmed/{sample}-{unit}-2.fastq.gz",
        json=RESULTDIR+"/qc/{sample}-{unit}.fastp.json",
        html=RESULTDIR+"/qc/{sample}-{unit}.html"
    params:
        extra =config["params"]["fastp"]["extra"] 
    resources:
        mem_mb = config["params"]["fastp"]["mem_mb"]
    threads:
        config["params"]["fastp"]["threads"]
    log:
        RESULTDIR+"/logs/fastp/{sample}-{unit}.log"
    script:
        "../scripts/fastp_pe.py"
                
        
# rule fastqc_post_se:
#     input:
#         get_trimmed
#     output:
#         html=RESULTDIR+"/qc/fastqc_post/{sample}-{unit}.html",
#         zip=RESULTDIR+"/qc/fastqc_post/{sample}-{unit}_fastqc.zip" # the suffix _fastqc.zip is necessary for multiqc
#     params: ""
#     log:
#         RESULTDIR+"/logs/fastqc_post/{sample}-{unit}.log"
#     threads: config["params"]["fastqc"]["threads"]
#     resources:
#         mem_mb = config['params']['fastqc']['mem_mb']
#     script:
#         "../scripts/fastqc_wrapper_se.py"
# rule fastqc_pre_pe:
#     input:
#         get_fastqs
#     output:
#         html1=RESULTDIR+"/qc/fastqc_pre/{sample}-{unit}-1.html",
#         zip1=RESULTDIR+"/qc/fastqc_pre/{sample}-{unit}-1_fastqc.zip",
#         html2=RESULTDIR+"/qc/fastqc_pre/{sample}-{unit}-2.html",
#         zip2=RESULTDIR+"/qc/fastqc_pre/{sample}-{unit}-2_fastqc.zip" # the suffix _fastqc.zip is necessary for multiqc 
#     params: ""
#     log:
#         l1 =RESULTDIR+"/logs/fastqc_pre/{sample}-{unit}-1.log",
#         l2 = RESULTDIR+"/logs/fastqc_pre/{sample}-{unit}-2.log"
#     threads: config["params"]["fastqc"]["threads"]
#     resources:
#         mem_mb = config['params']['fastqc']['mem_mb']
#     script:
#         "../scripts/fastqc_wrapper_pe.py"



# rule fastqc_post_pe:
#     input:
#         get_trimmed
#     output:
#         html1 = RESULTDIR+"/qc/fastqc_post/{sample}-{unit}-1.html",
#         zip1 = RESULTDIR+"/qc/fastqc_post/{sample}-{unit}-1_fastqc.zip",
#         html2 = RESULTDIR+"/qc/fastqc_post/{sample}-{unit}-2.html",
#         zip2=RESULTDIR+"/qc/fastqc_post/{sample}-{unit}-2_fastqc.zip" # the suffix _fastqc.zip is necessary for multiqc
#     params: ""
#     log:
#         l1 = RESULTDIR+"/logs/fastqc_pre/{sample}-{unit}-1.log",
#         l2 = RESULTDIR+"/logs/fastqc_pre/{sample}-{unit}-2.log"
#     threads: config["params"]["fastqc"]["threads"]
#     resources:
#         mem_mb = config['params']['fastqc']['mem_mb']
#     script:
#         "../scripts/fastqc_wrapper_pe.py"
