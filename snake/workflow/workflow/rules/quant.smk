if config['paired']:
    ruleorder: kallisto_quant_pe > kallisto_quant_se
else:
    ruleorder: kallisto_quant_se > kallisto_quant_pe
RESULTDIR=config['resultdir']
#def get_genomes(wildcards):
#    """Get genomes files from genome sheet."""
#    fs = genomes['ffn']
#    return list(fs)


rule kallisto_index:
    input:
        fasta =  expand("{ffn}",ffn = genomes['ffn'])
    output:
        index = RESULTDIR+"/genomes/index.idx"
    params:
        extra = "--kmer-size=31"
    log:
        RESULTDIR+"/logs/kallisto/kallisto_index.log"
    threads: config['params']['kallisto_index']['threads']
    resources:
        mem_mb = config['params']['kallisto_index']['mem_mb']
    shell:
        "ml kallisto;"
        "mkdir -p {RESULTDIR}/kallisto/; "
        "kallisto index -i {output.index} {input.fasta} > {log}"

rule kallisto_quant_pe:
    input:
        fq1 = RESULTDIR+ "/trimmed/{sample}-{unit}-1.fastq.gz",
        fq2 = RESULTDIR +"/trimmed/{sample}-{unit}-2.fastq.gz",
        index = RESULTDIR+"/genomes/index.idx"
    output:
        path = directory(RESULTDIR+'/kallisto/quant_{sample}-{unit}'),
        tsv = RESULTDIR+'/kallisto/quant_{sample}-{unit}/abundance.tsv',
        h5 = RESULTDIR+'/kallisto/quant_{sample}-{unit}/abundance.h5',
        json = RESULTDIR+'/kallisto/quant_{sample}-{unit}/run_info.json'
    params:
        bootstrap = 100,
        extra = config['params']['kallisto_quant']['extra']
    log:
        RESULTDIR+"/logs/kallisto/quant_{sample}-{unit}.log"
    threads:  config['params']['kallisto_quant']['threads']
    resources:
        mem_mb = config['params']['kallisto_quant']['mem_mb']
    run:
        shell("ml kallisto; \
        mkdir -p {RESULTDIR}/kallisto/; \
        kallisto quant -i {input.index} -o {output.path} \
        -b {params.bootstrap} \
        {params.extra} \
        --threads {threads} \
        {input.fq1} {input.fq2}  2> {log}")

rule kallisto_quant_se:
    input:
        fq1 = RESULTDIR+ "/trimmed/{sample}-{unit}.fastq.gz",
        index = RESULTDIR+"/genomes/index.idx"
    output:
        path = directory(RESULTDIR+'/kallisto/quant_{sample}-{unit}'),
        tsv = RESULTDIR+'/kallisto/quant_{sample}-{unit}/abundance.tsv',
        h5 = RESULTDIR+'/kallisto/quant_{sample}-{unit}/abundance.h5',
        json = RESULTDIR+'/kallisto/quant_{sample}-{unit}/run_info.json'
    params:
        bootstrap = 100,
        extra = config['params']['kallisto_quant']['extra']
    log:
        RESULTDIR+"/logs/kallisto/quant_{sample}-{unit}.log"
    threads:  config['params']['kallisto_quant']['threads']
    resources:
        mem_mb = config['params']['kallisto_quant']['mem_mb']
    run:
        mean = units.loc[(wildcards.sample, wildcards.unit),'fragment_len_mean']
        sd = units.loc[(wildcards.sample, wildcards.unit),'fragment_len_sd']
        shell("ml kallisto; \
        mkdir -p {RESULTDIR}/kallisto/; \
        kallisto quant -i {input.index} -o {output.path} \
        --single -l {mean} -s {sd} \
        -b {params.bootstrap} \
        {params.extra} \
        --threads {threads} \
        {input.fq1}  2> {log}")


rule kallisto_summary:
    input:
        tsvs = [RESULTDIR+'/kallisto/quant_'+unit.sample+'-'+unit.unit+'/abundance.tsv' for unit in units.itertuples()]
    output:
        summary = RESULTDIR+'/kallisto_summary.txt'
    log:
        RESULTDIR+"/logs/kallisto/kallisto_summary.log"
    resources:
        mem_mb = config['params']['kallisto_summary']['mem_mb']
    run:
        sample_unit = lambda c: re.sub("quant_", "", [q for q in c.split("/") if q.startswith('quant_')][0])
        sample = lambda c: c.split("-")[0]
        types = {'target_id':'str','length':'int64','eff_length':'float64','est_counts':'float64','tpm':'float64'}
        use =  ['target_id','est_counts', 'tpm']
        def readtsv(tsv):
            su = sample_unit(tsv)
            d = {'tpm':su+"_tpm", "est_counts": su+"_estcounts"}
            df = pd.read_csv(tsv, sep = "\t", header = 0,index_col = 0,usecols = use, dtype = types)
            df = df.rename(columns= d)
            return(df)
        sample = lambda c: c.split("-")[0]
        dfs = [ readtsv(tsv) for tsv in input.tsvs]
        unit_prefixes = [sample_unit(tsv) for tsv in input.tsvs]
        sample_prefixes = [sample(su) for su in unit_prefixes]
        df = pd.concat(dfs, axis = 1)
        get_sample_type = lambda x: "_".join([x.split("-")[0], x.split("_")[-1]])
        sample_df = df.groupby(by = get_sample_type, axis = 1).sum()
        sample_df = df.to_csv(output.summary, '\t')

