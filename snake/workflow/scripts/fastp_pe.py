"""Snakemake wrapper for trimming unpaired reads using trim_galore."""

__author__ = "Kerrin Mendler"
__copyright__ = "Copyright 2018, Kerrin Mendler"
__email__ = "mendlerke@gmail.com"
__license__ = "MIT"


from snakemake.shell import shell
import os.path
import glob
from tempfile import TemporaryDirectory
def basename_without_ext(file_path):
    """Returns basename of file path, without the file extension."""

    base = os.path.basename(file_path)

    split_ind = 2 if base.endswith(".fastq.gz") else 1
    base = ".".join(base.split(".")[:-split_ind])

    return base

log = snakemake.log_fmt_shell()

n = len(snakemake.input)
assert n == 2, "Input must contain 2 (paired-end) elements."




shell("eval $( spack load --sh fastp ); "
      "(fastp "
      " --thread {snakemake.threads} "
      " {snakemake.params.extra} "
      " -i {snakemake.input.fq[0]} -I {snakemake.input.fq[1]} "
      " --out1 {snakemake.output.fq1} --out2 {snakemake.output.fq2} "
      " -j {snakemake.output.json} -h {snakemake.output.html} ) 2> {snakemake.log};")

