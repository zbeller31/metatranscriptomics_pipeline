"""Snakemake wrapper for fastqc."""

__author__ = "Julian de Ruiter"
__copyright__ = "Copyright 2017, Julian de Ruiter"
__email__ = "julianderuiter@gmail.com"
__license__ = "MIT"


from os import path
from tempfile import TemporaryDirectory

from snakemake.shell import shell

log = snakemake.log_fmt_shell(stdout=False, stderr=True)


def basename_without_ext(file_path):
    """Returns basename of file path, without the file extension."""

    base = path.basename(file_path)

    split_ind = 2 if base.endswith(".fastq.gz") else 1
    base = ".".join(base.split(".")[:-split_ind])

    return base


# Run fastqc, since there can be race conditions if multiple jobs
# use the same fastqc dir, we create a temp dir.
with TemporaryDirectory() as tempdir:
    shell(
        "ml fastqc;"
        "(fastqc {snakemake.params} --quiet -t {snakemake.threads} "
        "--outdir {tempdir:q} {snakemake.input[0]:q}) > {snakemake.log.l1}"
    )

    # Move outputs into proper position.
    output_base1 = basename_without_ext(snakemake.input[0])
    html_path1 = path.join(tempdir, output_base1 + "_fastqc.html")
    zip_path1 = path.join(tempdir, output_base1 + "_fastqc.zip")

    if snakemake.output.html1 != html_path1:
        shell("mv {html_path1:q} {snakemake.output.html1:q}")

    if snakemake.output.zip1 != zip_path1:
        shell("mv {zip_path1:q} {snakemake.output.zip1:q}")

with TemporaryDirectory() as tempdir:
    shell(
        "ml fastqc;"
        "(fastqc {snakemake.params} --quiet -t {snakemake.threads} "
        "--outdir {tempdir:q} {snakemake.input[1]:q}) > {snakemake.log.l2}"
    )

    # Move outputs into proper position.
    output_base2 = basename_without_ext(snakemake.input[1])
    html_path2 = path.join(tempdir, output_base2 + "_fastqc.html")
    zip_path2 = path.join(tempdir, output_base2 + "_fastqc.zip")

    if snakemake.output.html2 != html_path2:
        shell("mv {html_path2:q} {snakemake.output.html2:q}")

    if snakemake.output.zip2 != zip_path2:
        shell("mv {zip_path2:q} {snakemake.output.zip2:q}")
