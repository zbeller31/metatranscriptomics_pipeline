import yaml
import shutil
RESULTDIR=config['resultdir']

#prefastq_output = [RESULTDIR+"/qc/fastqc_pre/"+unit.sample+"-"+unit.unit+"_fastqc.zip" if is_single_end(unit.sample, unit.unit) else [RESULTDIR+"/qc/fastqc_pre/"+unit.sample+"-"+unit.unit+"-1_fastqc.zip", RESULTDIR+"/qc/fastqc_pre/"+unit.sample+"-"+unit.unit+"-2_fastqc.zip"] for unit in units.itertuples()]
#postfastq_output = [RESULTDIR+"/qc/fastqc_post/"+unit.sample+"-"+unit.unit+"_fastqc.zip" if is_single_end(unit.sample, unit.unit) else  RESULTDIR+"/qc/fastqc_post/"+unit.sample+"-"+unit.unit+"_fastqc.zip" for unit in units.itertuples()]
fastp_output = [RESULTDIR+"/qc/"+unit.sample+"-"+unit.unit+".fastp.json"  for unit in units.itertuples()]

kallisto_output = expand(RESULTDIR+"/logs/kallisto/quant_{unit.sample}-{unit.unit}.log", unit=units.itertuples())

# rule multiqc_pre:
#     input:
#         prefastq_output
#     output:
#         RESULTDIR+"/qc/multiqc_pre.html"
#     params:
#         ""  # Optional: extra parameters for multiqc.
#     log:
#         RESULTDIR+"/logs/multiqc_pre.log"
#     script:
#         "../scripts/multiqc_wrapper.py"
# rule multiqc_post:
#     input:
#         postfastq_output,kallisto_output
#     output:
#         RESULTDIR+"/multiqc_report.html"
#     params:
#         " -c workflow/schemas/multiqc.yaml "  # Optional: extra parameters for multiqc.
#     log:
#         RESULTDIR+"/logs/multiqc_post.log"
#     script:
#         "../scripts/multiqc_wrapper.py"


rule multiqc_post:
    input:
        fastp_output,kallisto_output
    output:
        RESULTDIR+"/multiqc_report.html"
    params:
        " -c workflow/schemas/multiqc.yaml "  # Optional: extra parameters for multiqc.
    resources:
        mem_mb=config['params']['multiqc']['mem_mb']
    log:
        RESULTDIR+"/logs/multiqc_post.log"
    script:
        "../scripts/multiqc_wrapper.py"

rule copy_config:
    input:
        units = config['units'],
        samples = config['samples']
    output:
        config= RESULTDIR+"/config.yaml",
        units = RESULTDIR+"/"+os.path.basename(config['units']),
        samples = RESULTDIR+"/"+os.path.basename(config['samples'])

    run:
        with open(output.config, 'w') as cc:
            yaml.dump(config,cc, default_flow_style=True)
        shutil.copyfile(input.units, output.units)
        shutil.copyfile(input.samples, output.samples)
