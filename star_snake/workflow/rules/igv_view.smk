rule igv_index:
    input:
        sam = "results/star/{sample}-{unit}.sam"
    output:
        # see STAR manual for additional output files
        sorted = "results/igv/{sample}-{unit}.sorted.sam",
        sai = "results/star/{sample}-{unit}.sorted.sam.sai"
    log:
        "results/logs/igv/{sample}-{unit}.log"
    threads:
        config["params"]["igv_index"]["threads"]
    script:
        "../scripts/igv_wrapper.py"
