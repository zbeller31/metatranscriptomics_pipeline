ruleorder: trim_galore_pe > trim_galore_se
ruleorder: fastqc_pre_pe > fastqc_pre_se

rule fastqc_pre_se:
	input:
		get_fastqs
	output:
		html="results/qc/fastqc_pre/{sample}-{unit}.html",
		zip="results/qc/fastqc_pre/{sample}-{unit}_fastqc.zip" # the suffix _fastqc.zip is necessary for multiqc to find the file. If not using multiqc, you are free to choose an arbitrary filename
	params: ""
	log:
		"results/logs/fastqc_pre/{sample}-{unit}.log"
	threads: config["params"]["fastqc"]["threads"]
	script:
		"../scripts/fastqc_wrapper_se.py"
rule trim_galore_se:
	input:
		get_fastqs
	output:
		fq="results/trimmed/{sample}-{unit}.fastq.gz",
		report="results/trimmed/{sample}-{unit}.fastq.gz_trimming_report.txt"
	params:
		extra=config["params"]["trim_galore"]["extra"], unit = "{unit}"

	threads: config["params"]["trim_galore"]["threads"]
	log:
		"results/logs/trim_galore/{sample}-{unit}.log"
	script:
		"../scripts/trim_galore_se_wrapper.py"
rule fastqc_post_se:
	input:
		get_trimmed
	output:
		html="results/qc/fastqc_post/{sample}-{unit}.html",
		zip="results/qc/fastqc_post/{sample}-{unit}_fastqc.zip" # the suffix _fastqc.zip is necessary for multiqc to find the file. If not using multiqc, you are free to choose an arbitrary filename
	params: ""
	log:
		"results/logs/fastqc_post/{sample}-{unit}.log"
	threads: config["params"]["fastqc"]["threads"]
	script:
		"../scripts/fastqc_wrapper_se.py"
rule fastqc_pre_pe:
	input:
		get_fastqs
	output:
		html1="results/qc/fastqc_pre/{sample}-{unit}-1.html",
		zip1="results/qc/fastqc_pre/{sample}-{unit}-1_fastqc.zip",
		html2="results/qc/fastqc_pre/{sample}-{unit}-2.html",
		zip2="results/qc/fastqc_pre/{sample}-{unit}-2_fastqc.zip" # the suffix _fastqc.zip is necessary for multiqc to find the file. If not using multiqc, you are free to choose an arbitrary filename
	params: ""
	log:
		l1 ="results/logs/fastqc_pre/{sample}-{unit}-1.log",
		l2 = "results/logs/fastqc_pre/{sample}-{unit}-2.log"
	threads: config["params"]["fastqc"]["threads"]
	script:
		"../scripts/fastqc_wrapper_pe.py"

rule trim_galore_pe:
	input:
		get_fastqs
	output:
		fq1 = "results/trimmed/{sample}-{unit}-1.fastq.gz",
		rep1 = "results/trimmed/{sample}-{unit}.1.fastq.gz_trimming_report.txt",
		rep2 = "results/trimmed/{sample}-{unit}.2.fastq.gz_trimming_report.txt",
		fq2 = "results/trimmed/{sample}-{unit}-2.fastq.gz"
	params:
		extra = config["params"]["trim_galore"]["extra"],unit = "{unit}"
	threads: config["params"]["trim_galore"]["threads"]
	log:
		"results/logs/trim_galore/{sample}-{unit}.log"
	script:
		"../scripts/trim_galore_pe_wrapper.py"

rule fastqc_post_pe:
	input:
		get_trimmed
	output:
		html1 = "results/qc/fastqc_post/{sample}-{unit}-1.html",
		zip1 = "results/qc/fastqc_post/{sample}-{unit}-1_fastqc.zip",
		html2 = "results/qc/fastqc_post/{sample}-{unit}-2.html",
		zip2="results/qc/fastqc_post/{sample}-{unit}-2_fastqc.zip" # the suffix _fastqc.zip is necessary for multiqc to find the file. If not using multiqc, you are free to choose an arbitrary filename
	params: ""
	log:
		l1 ="results/logs/fastqc_pre/{sample}-{unit}-1.log",
		l2 = "results/logs/fastqc_pre/{sample}-{unit}-2.log"
	threads: config["params"]["fastqc"]["threads"]
	script:
		"../scripts/fastqc_wrapper_pe.py"
