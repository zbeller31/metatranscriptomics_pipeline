import os
#need to figure out stranded counting
def kallisto_params(wildcards, input):
    extra = config["params"]["kallisto"]
    if len(input.fastq) == 1:
        extra += " --single"
        extra += (" --fragment-length {unit.fragment_len_mean} "
                  "--sd {unit.fragment_len_sd}").format(
                    unit=units.loc[
                        (wildcards.sample, wildcards.unit)])
    else:
        extra += " --fusion"
    return extra

rule star_index:
    input:
        fasta = expand("{fna}", fna = genomes['fna'])
    output:
        directory("results/genomes")
    message:
        "Testing STAR index"
    threads:
        8
    resources: 
        mem_mb=100000
    params:
        extra = "",
        read_length=config['params']['star_index']['read_length'],
        threads=config['params']['star_index']['threads']
    log:
        "results/logs/star/star_index.log"
    script:
        "../scripts/star_index_wrapper.py"



rule star_map:
    input:
        fqs = get_trimmed,
	index = "results/genomes"
    output:
        # see STAR manual for additional output files
        bam = "results/star/{sample}-{unit}.bam",
        lfo = "results/star/{sample}-{unit}_Log.final.out"
    resources: 
        mem_mb=20000
    log:
        "results/logs/star/{sample}-{unit}.log"
    params:
        # path to STAR reference genome index
        # optional parameters
        extra=""
    threads: config["params"]["star_map"]["threads"]
    script:
        "../scripts/star_map_wrapper.py"

rule compile_saf:
    input:
        get_gffs
    output:
        saf = "results/counts/master.saf",
        txt = 'results/counts/master.txt'
    message:
        "Compiling SAF"
    threads:
        1
    params:
        id = "id",
        filter = ['CDS', 'rRNA', 'tRNA', 'tmRNA']
    log:
        "results/logs/counts/saf.log"
    shell:
        "ml py-biopython; ml bcbio-gff; python workflow/scripts/compile_saf.py -g {input} -s {output.saf} -t {output.txt} -f {params.filter}"

rule feature_counts:
    input:
        bam="results/star/{sample}-{unit}.bam",
        saf = "results/counts/master.saf"
    output:
        counts="results/counts/{sample}-{unit}_counts.txt",
        summary="results/counts/{sample}-{unit}.summary"
    resources: 
        mem_mb=20000
    params:
        group='locus_tag',
        mapping_minimum="0",
	    extra=config["params"]["feature_counts"]["extra"],
        stranded=config["params"]["feature_counts"]["stranded"]
    threads:
        config["params"]["feature_counts"]["threads"]
    log:
        "results/logs/feature_counts/{sample}-{unit}.txt"
    script:
        "../scripts/feature_counts_wrapper.py"


rule merge_counts:
    input:
        counts = ["results/counts/"+unit.sample+"-"+unit.unit+"_counts.txt" for unit in units.itertuples()],
        txt = 'results/counts/master.txt'
    output:
        merged = "results/counts/merged.txt",
        type_count = "results/counts/type_count.txt",
        type_fraction = "results/counts/type_fraction.txt",
        sample_merged = "results/counts/sample_merged.txt"
    resources: 
        mem_mb=100000
    run:
        pr = lambda c: os.path.splitext(os.path.basename(c))[0]
        pg = lambda s: s.split('-')[0]
        # Merge count files.
        frames = (pd.read_csv(fp, sep="\t", skiprows=1,
                        index_col=list(range(6)))
            for fp in input.counts)
        txt = pd.read_csv(input.txt, sep="\t", index_col = list(range(2)))
        merged = pd.concat(frames, axis=1)
        merged.columns = [pr(c) for c in merged.columns]
        merged.to_csv(output.merged, sep="\t", index=True)
        # Extract sample names.
        groups = [pg(c) for c in merged.columns]
        merged_counts = {}
        unique_groups = set(groups)
        for ug in unique_groups:
                mask = [g == ug for g in groups]
                merged_counts[ug] = merged.loc[:,mask].sum(axis =1)
        merged_df = pd.concat(merged_counts, axis =1)
        merged.to_csv(output.merged, sep="\t", index=True)
        typed = merged_df.merge(txt, how = 'left',  left_on =['Geneid', 'Chr'], right_index=True)
        cds_mask = typed['FeatureType']=='CDS'
        merged_cds = typed[cds_mask].drop(columns = 'FeatureType')
        merged_cds.to_csv(output.merged, sep="\t", index=True)
        type_count = typed.groupby(by = 'FeatureType').sum()
        type_count.to_csv(output.type_count, sep="\t", index=True)
        type_fraction = type_count/type_count.sum(axis=0)
        type_fraction.to_csv(output.type_fraction, sep="\t", index=True)
        merged_df.to_csv(output.sample_merged, sep="\t", index=True)
