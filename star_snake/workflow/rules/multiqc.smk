prefastq_output = ["results/qc/fastqc_pre/"+unit.sample+"-"+unit.unit+"_fastqc.zip" if is_single_end(unit.sample, unit.unit) else ["results/qc/fastqc_pre/"+unit.sample+"-"+unit.unit+"-1_fastqc.zip", "results/qc/fastqc_pre/"+unit.sample+"-"+unit.unit+"-2_fastqc.zip"] for unit in units.itertuples()]
postfastq_output = ["results/qc/fastqc_post/"+unit.sample+"-"+unit.unit+"_fastqc.zip" if is_single_end(unit.sample, unit.unit) else  "results/qc/fastqc_post/"+unit.sample+"-"+unit.unit+"_fastqc.zip" for unit in units.itertuples()]


star_output = expand("results/star/{unit.sample}-{unit.unit}_Log.final.out", unit=units.itertuples())
fc_output = expand("results/counts/{unit.sample}-{unit.unit}.summary", unit=units.itertuples())

rule multiqc_pre:
    input:
        prefastq_output
    output:
        "results/qc/multiqc_pre.html"
    params:
        ""  # Optional: extra parameters for multiqc.
    log:
        "results/logs/multiqc_pre.log"
    script:
        "../scripts/multiqc_wrapper.py"
rule multiqc_post:
    input:
        postfastq_output,star_output,fc_output
    output:
        "results/multiqc_report.html"
    params:
        " -c workflow/schemas/multiqc.yaml "  # Optional: extra parameters for multiqc.
    log:
        "results/logs/multiqc_post.log"
    script:
        "../scripts/multiqc_wrapper.py"
