from snakemake.shell import shell
shell("ml py-biopython; ml bcbio-gff; python compile_saf.py -g {snakemake.input} -s {snakemake.output.saf} -t {snakemake.output.txt} -f {snakemake.params.filter}")
