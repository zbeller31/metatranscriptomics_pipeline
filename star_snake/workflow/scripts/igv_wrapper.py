import os
from snakemake.shell import shell

log = snakemake.log_fmt_shell(stdout=True, stderr=True)

assert len(snakemake.input.sam)>1, 'Read 1 is required input'


outprefix = os.path.splitext(snakemake.input.sam)[0]
sortedsam = os.path.join(outprefix+".sorted.sam"
sortedsamsai = outprefix+".sorted.sam.sai"
shell(
    "ml igvtools;"
    "igvtools sort {snakemake.input.sam} {snakemake.output.sorted} {log};"
    "igvtools index {snakemake.output.sorted} {log};"
)
