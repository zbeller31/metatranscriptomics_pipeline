__author__ = "Johannes Köster"
__copyright__ = "Copyright 2016, Johannes Köster"
__email__ = "koester@jimmy.harvard.edu"
__license__ = "MIT"


import os
from snakemake.shell import shell

extra = snakemake.params.get("extra", "")
log = snakemake.log_fmt_shell(stdout=True, stderr=True)

assert len(snakemake.input[0])>1, 'Read 1 is required input'
input_str = " ".join(snakemake.input.fqs)

if snakemake.input[0].endswith(".gz"):
    readcmd = "--readFilesCommand zcat"
else:
    readcmd = ""

outprefix = os.path.splitext(snakemake.output.bam)[0]+"_"
og_output = outprefix+"Aligned.sortedByCoord.out.bam"
shell(
    "ml star;"
    "STAR "
    "{extra} "
    "--runThreadN {snakemake.threads} "
    "--genomeDir {snakemake.input.index} "
    "--readFilesIn {input_str} "
    "{readcmd} "
    "--outFileNamePrefix {outprefix} "
    "--outSAMtype BAM SortedByCoordinate "
    "--outStd Log "
    "{log};"
    "mv {og_output} {snakemake.output.bam};"
)
