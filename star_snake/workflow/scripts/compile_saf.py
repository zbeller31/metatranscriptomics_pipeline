import argparse
from Bio import SeqIO
from Bio import Alphabet
from BCBio import GFF




def gen_saf(gffs, saf_out,saf_txt, filter, id_tag = 'locus_tag'):
    parse_strand = lambda s :  '+' if s>0 else  '-'
    limit_info = dict(gff_type = filter)
    with open(saf_txt, 'w') as txt:
        with open(saf_out, 'w') as w:
            w.write('GeneID\tChr\tStart\tEnd\tStrand\n')
            txt.write('GeneID\tChr\tFeatureType\n')
            for gff in gffs:
                for rec in GFF.parse(gff, limit_info=limit_info):
                    for feature in rec.features:
                        if id_tag == 'id':
                            w.write("\t".join([feature.id, rec.id, str(feature.location.start),str(feature.location.end), parse_strand(feature.strand)])+'\n')
                            txt.write("\t".join([feature.id, rec.id, str(feature.type)])+'\n')
                        elif id_tag =='locus_tag' and 'locus_tag' in feature.qualifiers:
                            w.write("\t".join([feature.qualifiers['locus_tag'][0], rec.id, str(feature.location.start),str(feature.location.end), parse_strand(feature.strand)])+'\n')
                            txt.write("\t".join([feature.qualifiers['locus_tag'][0], rec.id, str(feature.type)])+'\n')
                        else:
                            w.write("\t".join([feature.id, rec.id, str(feature.location.start),str(feature.location.end), parse_strand(feature.strand)])+'\n')
                            txt.write("\t".join([feature.id, rec.id, str(feature.type)])+'\n')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-g",'--gff',help="gff list",  required=True,  nargs='+')
    parser.add_argument("-s",'--saf',help="[STRING] output saf",  required=True,type=str)
    parser.add_argument("-t",'--txt',help="[STRING] output txt",  required=True,type=str)
    parser.add_argument("-i",'--id',help="[STRING] feature identifier",  required=False,default = 'locus_tag',type=str)
    parser.add_argument("-f",'--filter',help="list of features to include",  required=True, nargs='+')
    args = parser.parse_args()
    gen_saf(args.gff, args.saf,args.txt, args.filter, args.id)
