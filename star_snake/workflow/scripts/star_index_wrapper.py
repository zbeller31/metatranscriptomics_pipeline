"""Snakemake wrapper for STAR index"""

__author__ = "Thibault Dayris"
__copyright__ = "Copyright 2019, Dayris Thibault"
__email__ = "thibault.dayris@gustaveroussy.fr"
__license__ = "MIT"

from snakemake.shell import shell
from snakemake.utils import makedirs
from Bio import SeqIO
import math
import os
def get_length_and_ncontigs(fna_list):
    genome_length = 0
    ncontigs = 0
    for fna in fna_list:
        for rec in SeqIO.parse(fna, 'fasta'):
            ncontigs+=1
            genome_length+=len(rec.seq)
    return((genome_length, ncontigs))

log = snakemake.log_fmt_shell(stdout=True, stderr=True)
genome_length, ncontigs = get_length_and_ncontigs(snakemake.input.fasta)
sa = min(14, int(math.log2(genome_length)/2-1))
nbits = min(18, int(math.log2(max(genome_length/ncontigs,snakemake.params.read_length))))
sa_str = str(sa)
nbit_str = str(nbits)
extra = snakemake.params.get("extra", "")
sjdb_overhang = snakemake.params.get("sjdbOverhang", "100")

gtf = snakemake.input.get("gtf")
if gtf is not None:
    gtf = "--sjdbGTFfile " + gtf
    sjdb_overhang = "--sjdbOverhang " + sjdb_overhang
else:
    gtf = sjdb_overhang = ""

makedirs(snakemake.output)
output_dir = os.path.dirname(log[2:])+'/'
shell(
    "ml star;"
    "STAR "  # Tool
    "--runMode genomeGenerate "  # Indexation mode
    "--outFileNamePrefix {output_dir} " 
    "--genomeSAindexNbases {sa_str} "
    "--genomeChrBinNbits {nbit_str} "
    "--alignIntronMax 1 "
    "{extra} "  # Optional parameters
    "--runThreadN {snakemake.threads} "  # Number of threads
    "--genomeDir {snakemake.output} "  # Path to output
    "--genomeFastaFiles {snakemake.input.fasta} "  # Path to fasta files
    "{sjdb_overhang} "  # Read-len - 1
    "{gtf} "  # Highly recommended GTF
    "{log} "  # Logging
)
