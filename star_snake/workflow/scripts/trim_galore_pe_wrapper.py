"""Snakemake wrapper for trimming paired-end reads using trim_galore."""

__author__ = "Kerrin Mendler"
__copyright__ = "Copyright 2018, Kerrin Mendler"
__email__ = "mendlerke@gmail.com"
__license__ = "MIT"


from snakemake.shell import shell
import os.path

def basename_without_ext(file_path):
    """Returns basename of file path, without the file extension."""

    base = os.path.basename(file_path)

    split_ind = 2 if base.endswith(".fastq.gz") else 1
    base = ".".join(base.split(".")[:-split_ind])

    return base

log = snakemake.log_fmt_shell()

# Check that two input files were supplied
n = len(snakemake.input)
assert n == 2, "Input must contain 2 files. Given: %r." % n

# Don't run with `--fastqc` flag
if "--fastqc" in snakemake.params.get("extra", ""):
    raise ValueError(
        "The trim_galore Snakemake wrapper cannot "
        "be run with the `--fastqc` flag. Please "
        "remove the flag from extra params. "
        "You can use the fastqc Snakemake wrapper on "
        "the input and output files instead."
    )

# Check that four output files were supplied
m = len(snakemake.output)
assert m == 4, "Output must contain 4 files. Given: %r." % m
output_base1 = basename_without_ext(snakemake.input[0])
output_base2 = basename_without_ext(snakemake.input[1])
# Check that all output files are in the same directory
out_dir = os.path.dirname(snakemake.output[0])
pre_out_dir = out_dir+"/"+snakemake.params.unit+"/"
for file_path in snakemake.output[1:]:
    assert out_dir == os.path.dirname(file_path), (
        "trim_galore can only output files to a single directory."
        " Please indicate only one directory for the output files."
    )
shell(
    "ml trim_galore;"
    "(trim_galore"
    " {snakemake.params.extra}"
    " --paired"
    " -o {pre_out_dir}"
    " {snakemake.input})"
    " {log};"
"mv {pre_out_dir}{output_base1}_val_1.fq.gz {snakemake.output.fq1};"
"mv {pre_out_dir}{output_base2}_val_2.fq.gz {snakemake.output.fq2};"
"mv {pre_out_dir}{output_base1}.fastq.gz_trimming_report.txt {snakemake.output.rep1};"
"mv {pre_out_dir}{output_base2}.fastq.gz_trimming_report.txt {snakemake.output.rep2};"
)
