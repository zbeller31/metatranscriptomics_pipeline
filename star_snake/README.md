Snakemake workflow heavily pulling from https://github.com/snakemake-workflows/rna-seq-kallisto-sleuth

INPUT: genome, read/unit, and sample tables and FASTQs, FNAs and GFFs for genome alignment
Output:
FastQC - QC of untrimmed reads
TrimGalore - Adapter/Quality trimmed reads
FastQC - QC of trimmed reads
STAR - SAM and Log files describing read alignment
featureCounts - count feature/aligned read overlap
merged table - python code to merge the count tables for each unit
multiqc summaries - a pre trimming multiqc and a post trimming html including mapping and quantifying stats

File organization:
star_snake - home directory
  config - contains tables describing input along with config.yaml file naming input settings
  workflow - contains all the working elements of the snakemake workflow
    rules - snakemake code describing declarative input output statements with some script/wrapper/python code to produce Output
    scripts - python scripts that will often wrap shell commands (many of these are stolen wrapper files with inserted module load calls)
    schemas - yaml files that describe the requirements for input tables, if tables don't satisfy these the workflow will throw error
    Snakefile - master rule file, includes all other rule files, has a target rule that acts as a declarative code endpoint (when a target is not provided on snakemake call)
