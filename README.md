# metatranscriptomics_pipeline

Snakemake pipelines to qc, filter/trim, map, and quantify meta-transcriptomic data.

Pipelines:
- SNAKE/ - contains snakemake project that will QC and process reads using Kallisto
- STAR_SNAKE/ - contains snakemake project that will process reads using STAR and feature counts
